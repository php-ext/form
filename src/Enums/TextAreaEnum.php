<?php
/**
 * Created by PhpStorm.
 * User: Oleg G.
 * Date: 09.05.2018
 * Time: 12:08
 */

namespace PhpExt\Form\Enums;

class TextAreaEnum extends AttrEnum
{
    const ATTR_NAME = 'name';
}